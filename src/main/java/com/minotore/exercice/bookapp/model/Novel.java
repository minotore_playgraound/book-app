package com.minotore.exercice.bookapp.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@DiscriminatorValue("NOV")
public class Novel extends Book {

    private String storySummary;

    protected Novel() {
        super();
    }

    public Novel(Long id, String name, Double price, Long totalUnitsSold, BookCategory category, Date publicationDate, int numberOfPages, Author author, String storySummary) {
        super(id, name, price, totalUnitsSold, category, publicationDate, numberOfPages, author);
        this.storySummary = storySummary;
    }

    public String getStorySummary() {
        return storySummary;
    }

    public void setStorySummary(String storySummary) {
        this.storySummary = storySummary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Novel)) return false;
        if (!super.equals(o)) return false;
        Novel novel = (Novel) o;
        return storySummary.equals(novel.storySummary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), storySummary);
    }

    @Override
    public String toString() {
        return "Novel{" +
                "storySummary='" + storySummary + '\'' +
                '}';
    }
}
