package com.minotore.exercice.bookapp.model;

public enum BookCategory {
    Historical, Crime, Fashion, Fiction, Cooking
}
