package com.minotore.exercice.bookapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int age;
    //
    @OneToMany(mappedBy = "author")
    @JsonIgnore
    private List<Book> bookList;

    protected Author() {
    }

    public Author(Long id, String name, int age, List<Book> bookList) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.bookList = bookList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;
        Author author = (Author) o;
        return age == author.age &&
                Objects.equals(id, author.id) &&
                Objects.equals(name, author.name) &&
                bookList.equals(author.bookList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age, bookList);
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", bookList=" + bookList +
                '}';
    }
}
