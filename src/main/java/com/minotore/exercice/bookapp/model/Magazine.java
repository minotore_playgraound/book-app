package com.minotore.exercice.bookapp.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@DiscriminatorValue("MAG")
public class Magazine extends Book {

    private Date nextReleaseDate;
    @ElementCollection
    private Set<String> keySubjects;

    protected Magazine() {
        super();
    }

    public Magazine(Long id, String name, Double price, Long totalUnitsSold, BookCategory category, Date publicationDate, int numberOfPages, Author author, Date nextReleaseDate, Set<String> keySubjects) {
        super(id, name, price, totalUnitsSold, category, publicationDate, numberOfPages, author);
        this.nextReleaseDate = nextReleaseDate;
        this.keySubjects = keySubjects;
    }

    public Date getNextReleaseDate() {
        return nextReleaseDate;
    }

    public void setNextReleaseDate(Date nextReleaseDate) {
        this.nextReleaseDate = nextReleaseDate;
    }

    public Set<String> getKeySubjects() {
        return keySubjects;
    }

    public void setKeySubjects(Set<String> keySubjects) {
        this.keySubjects = keySubjects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Magazine)) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return nextReleaseDate.equals(magazine.nextReleaseDate) &&
                keySubjects.equals(magazine.keySubjects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), nextReleaseDate, keySubjects);
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "nextReleaseDate=" + nextReleaseDate +
                ", keySubjects=" + keySubjects +
                '}';
    }
}
