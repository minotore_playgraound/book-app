package com.minotore.exercice.bookapp.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    //

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "book_library",
            joinColumns = @JoinColumn(name = "library_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"))
    private List<Book> bookList;

    protected Library() {
    }

    public Library(Long id, String name, String address, List<Book> bookList) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.bookList = bookList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Library)) return false;
        Library library = (Library) o;
        return id.equals(library.id) &&
                name.equals(library.name) &&
                address.equals(library.address) &&
                bookList.equals(library.bookList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, address, bookList);
    }

    @Override
    public String toString() {
        return "Library{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", bookList=" + bookList +
                '}';
    }
}
