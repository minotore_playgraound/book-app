package com.minotore.exercice.bookapp.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "book_type", discriminatorType = DiscriminatorType.STRING)
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Double price;
    private Long totalUnitsSold;
    private BookCategory category;
    private Date publicationDate;
    private int numberOfPages;
    //
    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    protected Book() {
    }

    public Book(Long id, String name, Double price, Long totalUnitsSold, BookCategory category, Date publicationDate, int numberOfPages, Author author) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.totalUnitsSold = totalUnitsSold;
        this.category = category;
        this.publicationDate = publicationDate;
        this.numberOfPages = numberOfPages;
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getTotalUnitsSold() {
        return totalUnitsSold;
    }

    public void setTotalUnitsSold(Long totalUnitsSold) {
        this.totalUnitsSold = totalUnitsSold;
    }

    public BookCategory getCategory() {
        return category;
    }

    public void setCategory(BookCategory category) {
        this.category = category;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return numberOfPages == book.numberOfPages &&
                id.equals(book.id) &&
                name.equals(book.name) &&
                price.equals(book.price) &&
                totalUnitsSold.equals(book.totalUnitsSold) &&
                category == book.category &&
                publicationDate.equals(book.publicationDate) &&
                author.equals(book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, totalUnitsSold, category, publicationDate, numberOfPages, author);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", totalUnitsSold=" + totalUnitsSold +
                ", category=" + category +
                ", publicationDate=" + publicationDate +
                ", numberOfPages=" + numberOfPages +
                ", author=" + author +
                '}';
    }
}
