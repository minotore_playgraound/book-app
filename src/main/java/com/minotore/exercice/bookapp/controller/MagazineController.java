package com.minotore.exercice.bookapp.controller;

import com.minotore.exercice.bookapp.model.Magazine;
import com.minotore.exercice.bookapp.service.MagazineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/v1/magazines")
public class MagazineController {
    @Autowired
    private MagazineService magazineService;

    @GetMapping
    public List<Magazine> getAllMagazines() {
        return magazineService.findAll();
    }

    @GetMapping("/closestReleaseDate")
    public Magazine getClosestReleaseDateMagazine() {
        return magazineService.findAll().stream()
                .sorted((magazine1, magazine2) -> magazine1.getNextReleaseDate().compareTo(magazine2.getNextReleaseDate()))
                .findFirst()
                .get();
    }
}
