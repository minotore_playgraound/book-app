package com.minotore.exercice.bookapp.controller;


import com.minotore.exercice.bookapp.model.Book;
import com.minotore.exercice.bookapp.model.Library;
import com.minotore.exercice.bookapp.model.Magazine;
import com.minotore.exercice.bookapp.model.Novel;
import com.minotore.exercice.bookapp.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/v1/librarys")
public class LibraryController {

    @Autowired
    private LibraryService libraryService;

    @GetMapping("/{library_id}")
    public Library getAllBooksByLibraryName(@PathVariable(value = "library_id") Long libraryId) {
        return libraryService.findById(libraryId);
    }

    @GetMapping("/books/{libraryName}")
    public ResponseEntity getAllBooksByLibraryName(@PathVariable(value = "libraryName") String name) {
        List<Book> bookList = libraryService.findAllBooksByLibraryName(name);
        if (bookList.isEmpty())
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        else
            return ResponseEntity.ok(bookList);
    }

    @GetMapping("/novels/{libraryName}")
    public List<Novel> getAllNovelsByLibraryName(@PathVariable(value = "libraryName") String name) {
        return libraryService.findAllBooksByLibraryName(name).stream()
                .filter(elem -> elem instanceof Novel)
                .map(elem -> (Novel) elem)
                .collect(Collectors.toList());
    }

    @GetMapping("/magazines/{libraryName}")
    public List<Magazine> getAllMagazinesByLibraryName(@PathVariable(value = "libraryName") String name) {
        return libraryService.findAllBooksByLibraryName(name).stream()
                .filter(elem -> elem instanceof Magazine)
                .map(elem -> (Magazine) elem)
                .collect(Collectors.toList());
    }
}
