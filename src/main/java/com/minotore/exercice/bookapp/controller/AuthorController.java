package com.minotore.exercice.bookapp.controller;


import com.minotore.exercice.bookapp.model.Book;
import com.minotore.exercice.bookapp.model.Novel;
import com.minotore.exercice.bookapp.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/v1/authors")
public class AuthorController {

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping("/{auther_id}/novels/mostSold")
    public Book getMostSoldNovelByAuthor(@PathVariable(value = "auther_id") Long authorId) {
        return authorRepository.findById(authorId).get().getBookList().stream()
                .filter(book -> book instanceof Novel)
                .sorted((novel1, novel2) -> novel2.getTotalUnitsSold().compareTo(novel1.getTotalUnitsSold()))
                .findFirst()
                .get();
    }
}
