package com.minotore.exercice.bookapp.repository;

import com.minotore.exercice.bookapp.model.Magazine;

import javax.transaction.Transactional;

@Transactional
public interface MagazineRepository extends BookBaseRepository<Magazine> {
}
