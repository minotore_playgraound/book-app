package com.minotore.exercice.bookapp.repository;

import com.minotore.exercice.bookapp.model.Novel;

import javax.transaction.Transactional;

@Transactional
public interface NovelRepository extends BookBaseRepository<Novel> {
}
