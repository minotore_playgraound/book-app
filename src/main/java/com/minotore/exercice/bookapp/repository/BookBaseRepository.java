package com.minotore.exercice.bookapp.repository;

import com.minotore.exercice.bookapp.model.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BookBaseRepository<T extends Book> extends CrudRepository<T, Long> {
}
