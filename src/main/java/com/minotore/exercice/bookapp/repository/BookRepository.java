package com.minotore.exercice.bookapp.repository;

import com.minotore.exercice.bookapp.model.Book;

import javax.transaction.Transactional;

@Transactional
public interface BookRepository extends BookBaseRepository<Book> {

}


