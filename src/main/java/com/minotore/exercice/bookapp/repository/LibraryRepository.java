package com.minotore.exercice.bookapp.repository;

import com.minotore.exercice.bookapp.model.Library;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LibraryRepository extends CrudRepository<Library, Long> {

    public Optional<Library> getLibraryByName(String name);

}
