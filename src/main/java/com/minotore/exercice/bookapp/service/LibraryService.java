package com.minotore.exercice.bookapp.service;

import com.minotore.exercice.bookapp.model.Book;
import com.minotore.exercice.bookapp.model.Library;
import com.minotore.exercice.bookapp.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LibraryService {

    @Autowired
    private LibraryRepository libraryRepository;

    public Library create(Library library) {
        return libraryRepository.save(library);
    }

    public Library findById(Long id) {
        return libraryRepository.findById(id).get();
    }

    public List<Library> findAll() {
        List<Library> libraryList = new ArrayList<>();
        libraryRepository.findAll().forEach(libraryList::add);
        return libraryList;
    }

    public void delete(Library library) {
        libraryRepository.delete(library);
    }

    public List<Book> findAllBooksByLibraryName(String libraryName) {
        final List<Book>[] bookList = new List[]{new ArrayList<>()};
        libraryRepository.getLibraryByName(libraryName).ifPresent(library -> bookList[0] = library.getBookList());
        return bookList[0];
    }

}
