package com.minotore.exercice.bookapp.service;

import com.minotore.exercice.bookapp.model.Magazine;
import com.minotore.exercice.bookapp.repository.MagazineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MagazineService {

    @Autowired
    private MagazineRepository magazineRepository;

    public Magazine create(Magazine magazine) {
        return magazineRepository.save(magazine);
    }

    public Magazine findById(Long id) {
        return magazineRepository.findById(id).get();
    }

    public List<Magazine> findAll() {
        List<Magazine> magazineList = new ArrayList<>();
        magazineRepository.findAll().forEach(magazineList::add);
        return magazineList;
    }

    public void delete(Magazine magazine) {
        magazineRepository.delete(magazine);
    }
}
