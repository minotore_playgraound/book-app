package com.minotore.exercice.bookapp.service;

import com.minotore.exercice.bookapp.model.Author;
import com.minotore.exercice.bookapp.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    public Author create(Author author) {
        return authorRepository.save(author);
    }

    public Author findById(Long id) {
        return authorRepository.findById(id).get();
    }

    public List<Author> findAll() {
        List<Author> authorList = new ArrayList<>();
        authorRepository.findAll().forEach(authorList::add);
        return authorList;
    }

    public void delete(Author author) {
        authorRepository.delete(author);
    }

}
