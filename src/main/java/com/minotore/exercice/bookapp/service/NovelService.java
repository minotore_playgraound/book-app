package com.minotore.exercice.bookapp.service;

import com.minotore.exercice.bookapp.model.Novel;
import com.minotore.exercice.bookapp.repository.NovelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NovelService {

    @Autowired
    private NovelRepository novelRepository;

    public Novel create(Novel novel) {
        return novelRepository.save(novel);
    }

    public Novel findById(Long id) {
        return novelRepository.findById(id).get();
    }

    public List<Novel> findAll() {
        List<Novel> novelList = new ArrayList<>();
        novelRepository.findAll().forEach(novelList::add);
        return novelList;
    }

    public void delete(Novel novel) {
        novelRepository.delete(novel);
    }
}
